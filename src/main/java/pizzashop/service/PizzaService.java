package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public void addPayment(int table, PaymentType type, double amount) {
        if ((table < 0 || table > 8) && amount < 0) {
            throw new RuntimeException("Invalid Table Number and Amount");
        }
        if (table < 0 || table > 8) {
            throw new RuntimeException("Invalid Table Number");
        }
        if (amount < 0) {
            throw new RuntimeException("Invalid Amount");
        }
        Payment payment = new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) {
        if (type == null) {
            throw new RuntimeException("Payment type is null");
        }
        double total = 0.0f;
        List<Payment> l = getPayments();
        if (l == null) {
            return total;
        }
        for (Payment p : l) {
            if (p.getType().equals(type))
                total += p.getAmount();
        }
        return total;
    }
}
