package pizzashop.repository;

import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PaymentRepositoryTest {
    private PaymentRepository paymentRepository = new PaymentRepository();

    @Test
    void testCase1() {

        Payment payment = mock(Payment.class);
        when(payment.validate())
                .thenReturn(true);
        Payment actual = paymentRepository.getPayment("4,CASH,13.97");
        assertEquals(actual.getAmount(),13.97);
        assertEquals(actual.getTableNumber(),4);
        assertEquals(actual.getType(), PaymentType.CASH);
    }

    @Test
    void testCase2() {
        Payment payment = mock(Payment.class);
        when(payment.validate())
                .thenReturn(true);
        Payment actual = paymentRepository.getPayment("4,CASH,13.97");
        assertNull(actual);
    }
}
