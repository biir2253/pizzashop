package pizzashop.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

class PizzaServiceTest {
    
    private MenuRepository menuRepository = Mockito.mock(MenuRepository.class);
    private PaymentRepository paymentRepository = Mockito.mock(PaymentRepository.class);

    private PizzaService pizzaService = new PizzaService(menuRepository, paymentRepository);

    @Test
    void testCase1() {
        final Payment mock = mock(Payment.class);
        
        doNothing().when(paymentRepository).add(any()); 

        pizzaService.addPayment(1, PaymentType.CASH, 100);
        
        verify(paymentRepository).add(any());
    }

    @Test
    void testCase2() {
        doNothing().when(paymentRepository).add(any());

        pizzaService.addPayment(1, PaymentType.CASH, 10);

        verify(paymentRepository).add(any());
    }

    @Test
    void testCase3() {
        doNothing().when(paymentRepository).add(any());

        pizzaService.addPayment(6, PaymentType.CASH, 100);

        verify(paymentRepository).add(any());
    }

    @Test
    void testCase4() {
        doNothing().when(paymentRepository).add(any());

        pizzaService.addPayment(8, PaymentType.CASH, 100);

        verify(paymentRepository).add(any());
    }

    @Test
    void testCase5() {
        doNothing().when(paymentRepository).add(any());

        Assertions.assertThrows(RuntimeException.class, () -> {
            pizzaService.addPayment(10, PaymentType.CASH, 10);
        }, "Invalid Table Number");
    }

    @Test
    void testCase6() {
        doNothing().when(paymentRepository).add(any());

        Assertions.assertThrows(RuntimeException.class, () -> {
            pizzaService.addPayment(1, PaymentType.CASH, -1);
        }, "Invalid Amount");
    }

    @Test
    void testCase7() {
        doNothing().when(paymentRepository).add(any());

        Assertions.assertThrows(RuntimeException.class, () -> {
            pizzaService.addPayment(-1, PaymentType.CASH, 0);
        }, "Invalid Table Number and Amount");
    }

    @Test
    void testCase8() {
        doNothing().when(paymentRepository).add(any());

        pizzaService.addPayment(1, PaymentType.CASH, 1);

        verify(paymentRepository).add(any());
    }

    @Test
    void testCase1WBT() {
        when(paymentRepository.getAll()).thenReturn(Collections.emptyList());

        final double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);
        
        assertEquals(0 ,totalAmount);
        verify(paymentRepository).getAll();
    }

    @Test
    void testCase2WBT() {
        when(paymentRepository.getAll()).thenReturn(Collections.emptyList());
        Assertions.assertThrows(RuntimeException.class, () -> {
            pizzaService.getTotalAmount(null);
        }, "Payment type is null");
    }

    @Test
    void testCase3WBT() {
        when(paymentRepository.getAll()).thenReturn(null);

        final double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);

        assertEquals(0 ,totalAmount);
        verify(paymentRepository).getAll();
    }

    @Test
    void testCase4WBT() {
        when(paymentRepository.getAll()).thenReturn(Collections.singletonList(new Payment(1, PaymentType.CARD, 10)));

        final double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);

        assertEquals(0 ,totalAmount);
        verify(paymentRepository).getAll();
    }

    @Test
    void testCase5WBT() {
        when(paymentRepository.getAll()).thenReturn(Collections.singletonList(new Payment(1, PaymentType.CASH, 10)));

        final double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);

        assertEquals(10 ,totalAmount);
        verify(paymentRepository).getAll();
    }
    @Test
    void testCase10() {

        int count = pizzaService.getPayments().size();
        pizzaService.addPayment(1, PaymentType.CASH, 1);

        assertEquals(count,pizzaService.getPayments().size());
    }

    @Test
    void testCase11() {
        int count = pizzaService.getPayments().size();
        pizzaService.addPayment(6, PaymentType.CASH, 1);

        assertEquals(count,pizzaService.getPayments().size());
    }
    
}